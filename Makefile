clean:
	cargo clean

build-debug: clean
	cargo build --verbose --all-features

build-release: clean
	cargo build --verbose

test-debug: build-debug
	cargo test --verbose --all-features

test-release: build-release
	cargo test --verbose

test: test-debug test-release