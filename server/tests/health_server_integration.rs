use http::StatusCode;

use server::{ApplicationConfiguration, HealthConfiguration};

#[tokio::test(flavor = "multi_thread", worker_threads = 2)]
async fn should_serve_health_endpoints() {
    let server_handle = tokio::spawn(server::run(ApplicationConfiguration {
        health: HealthConfiguration {
            port: 8080
        }
    }));
    let test_handle = tokio::spawn(async move {
        let ready = ureq::get("http://localhost:8080/health/ready")
            .call().unwrap();
        let live = ureq::get("http://localhost:8080/health/live")
            .call().unwrap();

        (ready.status(), live.status())
    });

    tokio::select! {
            _ = server_handle, if false => {}
            result = test_handle => {
                let (ready, live) = result.expect("Should have succeeded!");
                assert_eq!(ready, StatusCode::OK.as_u16());
                assert_eq!(live, StatusCode::OK.as_u16());
            }
        }
}