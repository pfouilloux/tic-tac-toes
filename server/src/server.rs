use std::sync::Arc;

use anyhow::Result;
use async_trait::async_trait;
use shaku::{Component, Interface};

use crate::health::HealthServer;

#[async_trait]
pub trait Server: Interface {
    async fn serve(&self);
}

#[derive(Component)]
#[shaku(interface = Server)]
pub struct ServerImpl {
    #[shaku(inject)]
    health_server: Arc<dyn HealthServer>
}

#[async_trait]
impl Server for ServerImpl {
    async fn serve(&self) {
        let health_server = self.health_server.clone();
        let health_handle = tokio::spawn(async move { health_server.serve().await });

        match tokio::try_join!(health_handle) {
            Ok(results) => log_errors(results),
            Err(e) => log::error!("Fatal error: {}", std::io::Error::from(e))
        }
    }
}

fn log_errors(results: (Result<()>, )) {
    let (health, ) = results;

    if let Err(e) = health {
        log::error!("Fatal error in health server: {}", e);
    }
}


#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use assertx::assert_logs_contain_in_order;
    use log::Level;

    use crate::{
        health::*,
        server::Server,
        server::ServerImpl,
    };

    #[tokio::test]
    async fn should_log_panic_in_health_server() {
        let logs = assertx::setup_logging_test();

        let mut health_server = MockHealthServer::new();
        health_server.expect_serve()
            .returning(|| panic!("Bad stuff happened!"));

        let server = ServerImpl {
            health_server: Arc::new(health_server)
        };

        server.serve().await;

        assert_logs_contain_in_order!(logs,
            Level::Error => "Fatal error: task panicked"
        );
    }

    #[tokio::test]
    async fn should_log_error_in_health_server() {
        let logs = assertx::setup_logging_test();

        let mut health_server = MockHealthServer::new();
        health_server.expect_serve()
            .returning(|| anyhow::bail!("Bad stuff happened!"));

        let server = ServerImpl {
            health_server: Arc::new(health_server)
        };

        server.serve().await;

        assert_logs_contain_in_order!(logs,
            Level::Error => "Fatal error in health server: Bad stuff happened!"
        );
    }
}