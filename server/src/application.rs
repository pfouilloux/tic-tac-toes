use shaku::HasComponent;

use crate::{
    health::{HealthModule, HealthServer},
    server::Server,
    server::ServerImpl,
};

shaku::module! {
    pub ApplicationModule {
        components = [ServerImpl],
        providers = [],
        use HealthModule {
            components = [HealthServer],
            providers = []
        }
    }
}

impl ApplicationModule {
    pub async fn run(&self) {
        let server: &dyn Server = self.resolve_ref();
        server.serve().await;
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use anyhow::Result;
    use async_trait::async_trait;
    use shaku::Component;

    use crate::health::*;

    use super::ApplicationModule;

    #[tokio::test]
    async fn should_start() {
        ApplicationModule::builder(Arc::new(FakeHealthModule::builder().build())).build().run().await;
    }

    shaku::module! {
        FakeHealthModule: HealthModule {
            components = [StubHealthServer],
            providers = []
        }
    }

    #[derive(Component)]
    #[shaku(interface = HealthServer)]
    struct StubHealthServer;

    #[async_trait]
    impl HealthServer for StubHealthServer {
        async fn serve(&self) -> Result<()> {
            Ok(())
        }
    }
}