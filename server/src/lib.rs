pub use crate::{
    application::ApplicationModule,
    configuration::ApplicationConfiguration,
    health::HealthConfiguration,
};

mod application;
mod configuration;
mod game;
mod health;
mod server;
mod shutdown;

pub async fn run(configuration: impl Into<ApplicationModule>) {
    configuration.into().run().await;
}
