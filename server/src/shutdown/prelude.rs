#[cfg(test)]
use mockall::automock;

#[cfg_attr(test, automock)]
pub trait ShutdownWatcher: Send + Sync {
    fn is_shutting_down(&self) -> bool;
}