use std::path::Path;

use clap::{clap_app, crate_authors, crate_description, crate_version};

use server::ApplicationConfiguration;

#[tokio::main]
async fn main() {
    let matches = clap_app!(server =>
        (version: crate_version!())
        (author: crate_authors!())
        (about: crate_description!())
        (@arg CONFIG: -c --config +takes_value "Sets a custom config file")
    ).get_matches();

    let local_config = matches.value_of("CONFIG").map(|it| Path::new(it.trim()));

    let config = ApplicationConfiguration::load(include_str!("../resources/default.toml"), local_config)
        .expect("Failed to load application config");

    server::run(config).await;
}