use shaku::Component;

use crate::health::HealthMonitor;

#[derive(Component)]
#[shaku(interface = HealthMonitor)]
pub struct DefaultHealthMonitor;

// TODO: Care about shutting down
impl HealthMonitor for DefaultHealthMonitor {
    fn ready(&self) -> bool {
        true
    }
    fn live(&self) -> bool {
        true
    }
}