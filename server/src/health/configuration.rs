use std::sync::Arc;

use serde::Deserialize;

use shared::networking::ToLocalIpv4Addr;

use crate::health::{HealthModule, monitor::DefaultHealthMonitor, warp_server::{WarpHealthServer, WarpHealthServerParameters}};

shaku::module! {
    HealthModuleImpl: HealthModule {
        components = [WarpHealthServer, DefaultHealthMonitor],
        providers = []
    }
}

#[derive(Deserialize, PartialEq, Debug)]
pub struct HealthConfiguration {
    pub port: u16,
}

impl From<HealthConfiguration> for Arc<dyn HealthModule> {
    fn from(config: HealthConfiguration) -> Self {
        Arc::new(HealthModuleImpl::builder()
            .with_component_parameters::<WarpHealthServer>(WarpHealthServerParameters {
                socket: config.port.to_local_addr()
            }).build())
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use crate::{health::HealthConfiguration, health::HealthModule};

    #[test]
    fn should_build_health_module_from_valid_configuration() {
        let config_str = "\
            port = 9999\n
        ";

        let config = toml::from_str::<HealthConfiguration>(config_str).expect("Should have deserialised");

        assert_eq!(config, HealthConfiguration {
            port: 9999
        });

        let _: Arc<dyn HealthModule> = config.into();
    }
}