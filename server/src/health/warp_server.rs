use std::{net::SocketAddr, sync::Arc};

use anyhow::Result;
use async_trait::async_trait;
use shaku::Component;
use warp::{Filter, http::StatusCode, Rejection};

use crate::health::{HealthMonitor, HealthServer};

#[derive(Component)]
#[shaku(interface = HealthServer)]
pub struct WarpHealthServer {
    socket: SocketAddr,
    #[shaku(inject)]
    monitor: Arc<dyn HealthMonitor>,
}

impl WarpHealthServer {
    fn live(&self) -> impl Filter<Extract=(StatusCode, ), Error=Rejection> + Clone {
        let monitor = self.monitor.clone();
        warp::get().and(warp::path!("health" / "live"))
            .map(move || if monitor.clone().live() { StatusCode::OK } else { StatusCode::SERVICE_UNAVAILABLE })
    }

    fn ready(&self) -> impl Filter<Extract=(StatusCode, ), Error=Rejection> + Clone {
        let monitor = self.monitor.clone();
        warp::get().and(warp::path!("health" / "ready"))
            .map(move || if monitor.clone().ready() { StatusCode::OK } else { StatusCode::SERVICE_UNAVAILABLE })
    }
}

#[async_trait]
impl HealthServer for WarpHealthServer {
    async fn serve(&self) -> Result<()> {
        warp::serve(self.live().or(self.ready())).bind(self.socket).await;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use std::{collections::HashMap, sync::{Arc, atomic::{AtomicU16, Ordering}}};

    use http::Method;
    use ureq::Error;
    use warp::http::StatusCode;

    use shared::networking::ToLocalIpv4Addr;

    use crate::{health::prelude::*, health::warp_server::WarpHealthServer};

    static COUNTER: AtomicU16 = AtomicU16::new(0);

    #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
    async fn should_accept_get_requests() {
        assert_server_endpoints(Arc::new(StubHealthMonitor),
                                Method::GET,
                                vec!["live".to_owned(), "ready".to_owned()],
                                StatusCode::OK).await
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
    async fn should_reject_options_request() {
        assert_server_endpoints(Arc::new(StubHealthMonitor),
                                Method::OPTIONS,
                                vec!["live".to_owned(), "ready".to_owned()],
                                StatusCode::METHOD_NOT_ALLOWED).await
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
    async fn should_reject_post_request() {
        assert_server_endpoints(Arc::new(StubHealthMonitor),
                                Method::POST,
                                vec!["live".to_owned(), "ready".to_owned()],
                                StatusCode::METHOD_NOT_ALLOWED).await
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
    async fn should_reject_put_request() {
        assert_server_endpoints(Arc::new(StubHealthMonitor),
                                Method::PUT,
                                vec!["live".to_owned(), "ready".to_owned()],
                                StatusCode::METHOD_NOT_ALLOWED).await
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
    async fn should_reject_delete_request() {
        assert_server_endpoints(Arc::new(StubHealthMonitor),
                                Method::DELETE,
                                vec!["live".to_owned(), "ready".to_owned()],
                                StatusCode::METHOD_NOT_ALLOWED).await
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
    async fn should_reject_head_request() {
        assert_server_endpoints(Arc::new(StubHealthMonitor),
                                Method::HEAD,
                                vec!["live".to_owned(), "ready".to_owned()],
                                StatusCode::METHOD_NOT_ALLOWED).await
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
    async fn should_reject_trace_request() {
        assert_server_endpoints(Arc::new(StubHealthMonitor),
                                Method::TRACE,
                                vec!["live".to_owned(), "ready".to_owned()],
                                StatusCode::METHOD_NOT_ALLOWED).await
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
    async fn should_reject_connect_request() {
        assert_server_endpoints(Arc::new(StubHealthMonitor),
                                Method::CONNECT,
                                vec!["live".to_owned(), "ready".to_owned()],
                                StatusCode::METHOD_NOT_ALLOWED).await
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 2)]
    async fn should_reject_patch_request() {
        assert_server_endpoints(Arc::new(StubHealthMonitor),
                                Method::PATCH,
                                vec!["live".to_owned(), "ready".to_owned()],
                                StatusCode::METHOD_NOT_ALLOWED).await
    }

    async fn assert_server_endpoints(monitor: Arc<dyn HealthMonitor>, method: Method, endpoints: Vec<String>, expected_status: StatusCode) {
        let port = 10444u16 + COUNTER.fetch_add(1, Ordering::SeqCst);
        let server = WarpHealthServer {
            socket: port.to_local_addr(),
            monitor,
        };

        let server_handle = tokio::spawn(async move { server.serve().await });

        let test_handle = tokio::spawn(async move {
            endpoints.iter()
                .map(|endpoint| {
                    let status_u16 = match ureq::request(method.as_str(), format!("http://localhost:{}/health/{}", port, endpoint).as_str()).call() {
                        Ok(r) => r.status(),
                        Err(e) => if let Error::Status(code, _) = e { code } else { 0 }
                    };
                    (endpoint.clone(), StatusCode::from_u16(status_u16).unwrap())
                }).collect::<HashMap<String, StatusCode>>()
        });

        tokio::select! {
            _ = server_handle, if false => {}
            result = test_handle => {
                for (key, actual) in result.expect("Should have succeeded!").iter() {
                    assert_eq!(actual, &expected_status, "{}", key);
                }
            }
        }
    }

    #[derive(Copy, Clone)]
    struct StubHealthMonitor;

    impl HealthMonitor for StubHealthMonitor {
        fn ready(&self) -> bool { true }
        fn live(&self) -> bool { true }
    }
}