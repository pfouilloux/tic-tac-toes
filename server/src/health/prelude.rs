use anyhow::Result;
use async_trait::async_trait;
#[cfg(test)]
use mockall::automock;
use shaku::{HasComponent, Interface};

pub trait HealthModule: HasComponent<dyn HealthServer> {}

#[cfg_attr(test, automock)]
pub trait HealthMonitor: Interface {
    fn ready(&self) -> bool;
    fn live(&self) -> bool;
}

#[cfg_attr(test, automock)]
#[async_trait]
pub trait HealthServer: Interface {
    async fn serve(&self) -> Result<()>;
}