use std::path::Path;

use anyhow::Result;
use config::{Config, File, FileFormat};
use serde::Deserialize;

use crate::{application::ApplicationModule, health::HealthConfiguration};

#[derive(Deserialize, PartialEq, Debug)]
pub struct ApplicationConfiguration {
    pub health: HealthConfiguration
}

impl ApplicationConfiguration {
    pub fn load(default: &str, local: Option<&Path>) -> Result<Self> {
        let mut config = Config::new();

        config.merge(File::from_str(default, FileFormat::Toml))?;

        if let Some(path) = local {
            config.merge(File::from(path))?;
        }

        Ok(config.try_into()?)
    }
}

impl From<ApplicationConfiguration> for ApplicationModule {
    fn from(config: ApplicationConfiguration) -> Self {
        ApplicationModule::builder(config.health.into()).build()
    }
}

#[cfg(test)]
mod tests {
    use std::io::Write;

    use crate::{application::ApplicationModule, configuration::ApplicationConfiguration, health::*};

    #[test]
    fn should_build_server_from_default_configuration() {
        let default = "\
            [health]\n
            port = 9999\n
        ";

        let config = ApplicationConfiguration::load(default, None).expect("Should have loaded resources");
        assert_eq!(config, ApplicationConfiguration {
            health: toml::from_str::<HealthConfiguration>("port = 9999").expect("Should have deserialised")
        });

        let _: ApplicationModule = config.into();
    }

    #[test]
    fn should_override_default_configuration() {
        let default = "\
            [health]\n
            port = 9999\n
        ";

        let mut local = tempfile::Builder::new()
            .prefix("tic-tac-toes")
            .rand_bytes(6)
            .suffix(".toml")
            .tempfile().expect("Should have built named temp file");
        local.write_all("\
            [health]\n
            port = 1010\n
        ".as_bytes()).expect("Should have written temp file");

        let config = ApplicationConfiguration::load(default, Some(local.path())).expect("Should have loaded resources");
        assert_eq!(config, ApplicationConfiguration {
            health: toml::from_str::<HealthConfiguration>("port = 1010").expect("Should have deserialised")
        });

        drop(local);

        let _: ApplicationModule = config.into();
    }
}