use std::{
    sync::Arc,
};

use anyhow::Result;
use async_trait::async_trait;

use protocol::Message;

use crate::{
    game::{
        GameServer,
        message::MessageHandler,
        network::MessageStream,
    },
    shutdown::ShutdownWatcher,
};

pub struct GameServerImpl {
    handler: Arc<dyn MessageHandler>,
    stream: Arc<dyn MessageStream>,
    shutdown_watcher: Arc<dyn ShutdownWatcher>,
}

#[async_trait]
impl GameServer for GameServerImpl {
    async fn serve(&self) -> Result<()> {
        while !self.shutdown_watcher.is_shutting_down() {
            if let Some(message) = self.stream.poll_next().await? {
                println!("ooh msg");
                let handler = self.handler.clone();

                tokio::spawn(handle(handler, message));

                println!("b4da yield");
                tokio::task::yield_now().await; //Let the handle task run first if there is no other thread to pick it up.
                println!("poop da loop");
            }
        }

        Ok(())
    }
}

async fn handle(handler: Arc<dyn MessageHandler>, message: Message) {
    //TODO: Measure number of message that couldn't be handled
    if let Err(e) = handler.handle(&message.header.msg_type, &message.payload).await {
        println!("handy wandy");
        log::warn!("Failed to handle message: {}. Reason: {}", format!("{:?}", message), e);
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use bytes::Bytes;
    use log::Level;
    use mockall::{predicate, Sequence};

    use protocol::*;

    use crate::{
        game::{server::GameServerImpl},
        game::GameServer,
        game::message::*,
        game::network::*,
        shutdown::*,
    };

    #[tokio::test]
    async fn should_handle_message() {
        let mut stream = MockMessageStream::new();
        let mut shutdown_watcher = MockShutdownWatcher::new();
        let mut handler = MockMessageHandler::new();

        let mut sequence = Sequence::new();

        shutdown_watcher.expect_is_shutting_down().once().in_sequence(&mut sequence).return_const(false);
        stream.expect_poll_next().once().in_sequence(&mut sequence)
            .returning(move || Ok(Some(create_test_message(42u8, Bytes::from_static(b"Hello World")))));
        handler.expect_handle()
            .with(predicate::eq(42u8), predicate::eq(Bytes::from_static(b"Hello World")))
            .once().in_sequence(&mut sequence).returning(|_, _| { Ok(()) });
        shutdown_watcher.expect_is_shutting_down().once().in_sequence(&mut sequence).return_const(true);

        let server = GameServerImpl {
            stream: Arc::new(stream),
            handler: Arc::new(handler),
            shutdown_watcher: Arc::new(shutdown_watcher),
        };

        server.serve().await.expect("Should have succeeded");
    }

    #[tokio::test]
    async fn should_log_and_continue_on_handler_error() {
        let logs = assertx::setup_logging_test();
        let mut handler = MockMessageHandler::new();
        let mut stream = MockMessageStream::new();
        let mut shutdown_watcher = MockShutdownWatcher::new();
        let mut sequence = Sequence::new();

        shutdown_watcher.expect_is_shutting_down().once().in_sequence(&mut sequence).return_const(false);
        stream.expect_poll_next().once().in_sequence(&mut sequence)
            .returning(move || Ok(Some(create_test_message(42u8, Bytes::from_static(b"Hello World")))));
        handler.expect_handle()
            .once().in_sequence(&mut sequence).returning(|_, _| { anyhow::bail!("Yeah, nah") });
        shutdown_watcher.expect_is_shutting_down().once().in_sequence(&mut sequence).return_const(true);

        let server = GameServerImpl {
            stream: Arc::new(stream),
            handler: Arc::new(handler),
            shutdown_watcher: Arc::new(shutdown_watcher),
        };

        server.serve().await.expect("Should have succeeded");

        assertx::assert_logs_contain_in_order!(logs,
            Level::Warn => format!("Failed to handle message: {:?}. Reason: Yeah, nah",
                create_test_message(42u8, Bytes::from_static(b"Hello World"))));
    }

    fn create_test_message(msg_type: u8, payload: Bytes) -> Message {
        Message {
            header: Header { checksum: 1234, check_value: 4321, msg_type },
            payload,
        }
    }
}