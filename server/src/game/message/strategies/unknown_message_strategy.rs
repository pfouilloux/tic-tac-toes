use anyhow::{anyhow, Result};
use bytes::Bytes;

use crate::game::message::MessageHandlingStrategy;

pub struct UnknownMessageStrategy;

impl MessageHandlingStrategy for UnknownMessageStrategy {
    fn handle(&self, _: &Bytes) -> Result<()> {
        Err(anyhow!("Unhandled message type"))
    }
}

#[cfg(test)]
mod tests {
    use bytes::Bytes;

    use crate::game::message::MessageHandlingStrategy;

    use super::UnknownMessageStrategy;

    #[test]
    fn should_return_error_with_message() {
        let error = UnknownMessageStrategy
            .handle(&Bytes::new())
            .expect_err("Should have errored!");

        assert_eq!(format!("{}", error), "Unhandled message type")
    }
}
