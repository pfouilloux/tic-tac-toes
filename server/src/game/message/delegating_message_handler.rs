use std::{collections::HashMap, sync::Arc};

use anyhow::{anyhow, Result};
use async_trait::async_trait;
use bytes::Bytes;

use super::{MessageHandler, MessageHandlingStrategy};

pub struct DelegatingMessageHandler {
    strategies: HashMap<u8, Arc<dyn MessageHandlingStrategy>>,
    fallback_strategy: Arc<dyn MessageHandlingStrategy>,
}

pub struct DelegatingMessageHandlerBuilder {
    strategies: HashMap<u8, Arc<dyn MessageHandlingStrategy>>,
    fallback_strategy: Option<Arc<dyn MessageHandlingStrategy>>,
}

#[async_trait]
impl MessageHandler for DelegatingMessageHandler {
    // TODO: Measure success/failure rate & speed
    async fn handle(&self, msg_type: &u8, payload: &Bytes) -> Result<()> {
        let strategy = self
            .strategies
            .get(msg_type)
            .unwrap_or(&self.fallback_strategy);

        handle_payload(strategy, payload)
    }
}

impl DelegatingMessageHandler {
    pub fn builder() -> DelegatingMessageHandlerBuilder {
        DelegatingMessageHandlerBuilder {
            fallback_strategy: None,
            strategies: HashMap::<u8, Arc<dyn MessageHandlingStrategy>>::new(),
        }
    }
}

impl DelegatingMessageHandlerBuilder {
    pub fn with_fallback_strategy(
        mut self,
        strategy: Arc<dyn MessageHandlingStrategy>,
    ) -> Self {
        self.fallback_strategy = Some(strategy.clone());
        self
    }

    pub fn with_strategy(
        mut self,
        msg_type: u8,
        strategy: Arc<dyn MessageHandlingStrategy>,
    ) -> Self {
        self.strategies.insert(msg_type, strategy.clone());
        self
    }

    pub fn build(self) -> Result<DelegatingMessageHandler> {
        Ok(DelegatingMessageHandler {
            strategies: self.strategies,
            fallback_strategy: self
                .fallback_strategy
                .ok_or(anyhow!("Please provide a fallback strategy"))?,
        })
    }
}

fn handle_payload(
    strategy: &Arc<dyn MessageHandlingStrategy>,
    payload: &Bytes,
) -> Result<()> {
    strategy.handle(payload)
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use bytes::Bytes;

    use crate::game::message::prelude::*;

    use super::DelegatingMessageHandler;

    #[tokio::test]
    async fn should_handle_test_message_using_test_message_strategy() {
        let fallback_strategy = MockMessageHandlingStrategy::new();
        let mut test_message_strategy = MockMessageHandlingStrategy::new();
        test_message_strategy
            .expect_handle()
            .times(1)
            .returning(|_| Ok(()));

        let handler = DelegatingMessageHandler::builder()
            .with_fallback_strategy(Arc::new(fallback_strategy))
            .with_strategy(42u8, Arc::new(test_message_strategy))
            .build()
            .expect("Should have built message handler");

        assert!(
            handler.handle(&42u8, &Bytes::new()).await.is_ok(),
            "Should have succeeded"
        )
    }

    #[tokio::test]
    async fn should_handle_unknown_message_using_fallback_strategy() {
        let mut mock_strategy = MockMessageHandlingStrategy::new();
        mock_strategy.expect_handle().times(1).returning(|_| Ok(()));

        let handler = DelegatingMessageHandler::builder()
            .with_fallback_strategy(Arc::new(mock_strategy))
            .build()
            .expect("Should have built message handler");

        assert!(
            handler.handle(&42u8, &Bytes::new()).await.is_ok(),
            "Should have succeeded"
        )
    }

    #[test]
    fn should_fail_to_build_delegating_message_handler_when_fallback_strategy_is_missing() {
        if let Err(error) = DelegatingMessageHandler::builder().build() {
            assert_eq!(
                format!("{}", error),
                "Please provide a fallback strategy"
            )
        } else {
            panic!("Should have failed!")
        }
    }
}
