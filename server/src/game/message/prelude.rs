use anyhow::Result;
use async_trait::async_trait;
use bytes::Bytes;
#[cfg(test)]
use mockall::automock;
use shaku::Interface;

#[cfg_attr(test, automock)]
pub trait MessageHandlingStrategy: Send + Sync {
    fn handle(&self, payload: &Bytes) -> Result<()>;
}

#[cfg_attr(test, automock)]
#[async_trait]
pub trait MessageHandler: Interface {
    async fn handle(&self, msg_type: &u8, payload: &Bytes) -> Result<()>;
}
