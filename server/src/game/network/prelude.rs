use std::net::SocketAddr;

use anyhow::Result;
use async_trait::async_trait;
use bytes::Bytes;
#[cfg(test)]
use mockall::automock;
use shaku::Interface;

use protocol::Message;

#[cfg_attr(test, automock)]
#[async_trait]
pub trait MessageStream: Interface {
    async fn poll_next(&self) -> Result<Option<Message>>;
}

#[cfg_attr(test, automock)]
#[async_trait]
pub trait Sender: Interface {
    async fn send(&self, recipient: &SocketAddr, bytes: &Bytes) -> Result<()>;
}

#[cfg_attr(test, automock)]
#[async_trait]
pub trait Receiver: Interface {
    async fn receive(&self) -> Result<Message>;
}