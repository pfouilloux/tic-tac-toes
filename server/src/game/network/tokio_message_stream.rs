use std::net::SocketAddr;

use anyhow::Result;
use async_trait::async_trait;
use shaku::Component;
use tokio::net::UdpSocket;

use protocol::Message;
use shared::networking::ToLocalIpv4Addr;

use crate::game::network::MessageStream;

#[derive(Component)]
#[shaku(interface = MessageStream)]
pub struct TokioMessageStream {
    socket: UdpSocket,
    buffer_size: usize,
}

impl TokioMessageStream {
    pub async fn new(port: u16, buffer_size: usize) -> Result<Self> {
        Ok(TokioMessageStream {
            socket: UdpSocket::bind(port.to_local_addr()).await?,
            buffer_size,
        })
    }
}

#[async_trait]
impl MessageStream for TokioMessageStream {
    async fn poll_next(&self) -> Result<Option<Message>> {
        // let mut buffer = BytesMut::with_capacity(self.buffer_size);
        //
        // self.socket.try_recv_from()
        Ok(None)
    }
}

// socket.readable().await?;
//
// // The buffer is **not** included in the async task and will
// // only exist on the stack.
// let mut buf = [0; 1024];
//
// // Try to recv data, this may still fail with `WouldBlock`
// // if the readiness event is a false positive.
// match socket.try_recv_from( & mut buf) {
// Ok((n, _addr)) => {
// println ! ("GOT {:?}", & buf[..n]);
// break;
// }
// Err( ref e) if e.kind() == io::ErrorKind::WouldBlock => {
// continue;
// }
// Err(e) => {
// return Err(e);
// }
// }

#[cfg(test)]
mod tests {
    use std::net::SocketAddr;

    use bytes::Bytes;
    use tokio::net::UdpSocket;

    use protocol::{Header, Message};
    use shared::networking::ToLocalIpv4Addr;

    use crate::game::{
        network::MessageStream,
        network::tokio_message_stream::TokioMessageStream,
    };

    #[tokio::test]
    async fn should_receive_messages() {
        let stream = TokioMessageStream::new(0, 256).await.expect("Should have created socket network");
        let client_socket = UdpSocket::bind(0.to_local_addr()).await.expect("Should have bound socket");

        client_socket.send_to(Message {
            header: Header {
                checksum: 1234,
                msg_type: 42,
                check_value: 4321,
            },
            payload: Bytes::from_static(b"Hello world"),
        }.into(), stream.socket.local_addr().expect("Should have gotten local address")).await
            .expect("Should have sent message");

        let message = stream.poll_next().await.expect("Should have polled successfully").expect("Should have received message");

        assert_eq!(message, Message {
            header: Header {
                checksum: 1234,
                msg_type: 42,
                check_value: 4321,
            },
            payload: Bytes::from_static(b"Hello world"),
        })
    }
}