use anyhow::Result;
use async_trait::async_trait;
#[cfg(test)]
use mockall::automock;
use shaku::{HasComponent, Interface};

pub trait GameModule: HasComponent<dyn GameServer> {}

#[cfg_attr(test, automock)]
#[async_trait]
pub trait GameServer: Interface {
    async fn serve(&self) -> Result<()>;
}