use std::net::{Ipv4Addr, SocketAddr};

pub trait ToLocalIpv4Addr {
    fn to_local_addr(&self) -> SocketAddr;
}

impl ToLocalIpv4Addr for u16 {
    fn to_local_addr(&self) -> SocketAddr {
        SocketAddr::new(Ipv4Addr::new(127, 0, 0, 1).into(), self.clone())
    }
}

#[cfg(test)]
mod tests {
    use std::net::Ipv4Addr;

    use crate::networking::ToLocalIpv4Addr;

    #[test]
    fn should_create_local_ipv4_socket_address_from_port() {
        let socket = 42.to_local_addr();

        assert!(socket.is_ipv4(), "Should be ipv4 socket");
        assert_eq!(socket.ip(), Ipv4Addr::new(127, 0, 0, 1));
        assert_eq!(socket.port(), 42);
    }
}