use std::net::SocketAddr;

pub trait Client {
    fn connect(&mut self, addr: SocketAddr);
}
