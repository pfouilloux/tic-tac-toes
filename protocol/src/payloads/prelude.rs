use bitpacked::bitpacked;

#[bitpacked]
pub struct ClientConnection {
    #[bits = 64]
    client_salt: u64
}

#[bitpacked]
pub struct ServerChallenge {
    #[bits = 64]
    server_salt: u64
}

#[bitpacked]
pub struct ClientChallengeResponse {
    #[bits = 64]
    session_key: u64
}

#[bitpacked]
pub struct Disconnect {
    #[bits = 64]
    session_key: u64
}