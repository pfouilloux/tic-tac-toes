use thiserror::Error;

#[derive(Error, Debug, PartialEq)]
#[error("message conversion error")]
pub enum MessageConversionError {
    #[error("message checksum {0} did not match computed checksum {1}, message is probably corrupt")]
    ChecksumMismatch(u32, u32),
    #[error("expected check value {1} but got {0}, client is probably out of date")]
    WrongCheckValue(u32, u32),
    #[error("message of size {0} is shorter than minimum header size")]
    IncompleteHeader(usize),
}

#[cfg(test)]
mod tests {
    use std::error::Error;
    use std::mem::size_of;

    use crate::{Header, MessageConversionError};

    #[test]
    fn should_display_checksum_mismatch_error_message() {
        assert_err_with_message(MessageConversionError::ChecksumMismatch(42, 90210),
                                "message checksum 42 did not match computed checksum 90210, message is probably corrupt")
    }

    #[test]
    fn should_display_check_value_error_message() {
        assert_err_with_message(MessageConversionError::WrongCheckValue(86, 91),
                                "expected check value 91 but got 86, client is probably out of date")
    }

    #[test]
    fn should_display_incomplete_header_error_message() {
        assert_err_with_message(MessageConversionError::IncompleteHeader(4),
                                "message of size 4 is shorter than minimum header size")
    }

    fn assert_err_with_message(err: impl Error, expected_msg: &str) {
        assert_eq!(format!("{}", err), expected_msg.to_owned())
    }
}