use std::{
    convert::{TryFrom, TryInto},
    ops::Range,
};

use bytes::{Buf, BufMut, Bytes, BytesMut};
use crc32fast::Hasher;

use crate::{
    Header,
    message::{
        errors::MessageConversionError,
        Message,
    },
};

const CHECK_VALUE: u32 = u32::from_be_bytes(*include_bytes!(concat!(env!("OUT_DIR"), "/check_value")));

type Result<T> = std::result::Result<T, MessageConversionError>;

impl TryFrom<Bytes> for Message {
    type Error = MessageConversionError;

    fn try_from(value: Bytes) -> Result<Self> {
        let checksum = u32::from_be_bytes(value.try_get(0..4)?);
        let calc_checksum = calc_checksum(value.get(4..).unwrap_or_default());
        if checksum != calc_checksum {
            return Err(MessageConversionError::ChecksumMismatch(checksum, calc_checksum));
        }

        let check_value = u32::from_be_bytes(value.try_get(4..8)?);
        if check_value != CHECK_VALUE {
            return Err(MessageConversionError::WrongCheckValue(check_value, CHECK_VALUE));
        }

        let msg_type = u8::from_be_bytes(value.try_get(8..9)?);

        Ok(Message {
            header: Header { checksum, check_value, msg_type },
            payload: Bytes::copy_from_slice(value.get(9..).unwrap_or_default()),
        })
    }
}

impl TryFrom<Message> for Bytes {
    type Error = MessageConversionError;

    fn try_from(value: Message) -> Result<Self> {
        if value.header.check_value != CHECK_VALUE {
            return Err(MessageConversionError::WrongCheckValue(value.header.check_value, CHECK_VALUE));
        }

        let mut checked_bytes = BytesMut::new();
        checked_bytes.put_u32(value.header.check_value);
        checked_bytes.put_u8(value.header.msg_type);
        checked_bytes.put(value.payload);

        let calc_checksum = calc_checksum(checked_bytes.chunk());
        if value.header.checksum != calc_checksum {
            return Err(MessageConversionError::ChecksumMismatch(value.header.checksum, calc_checksum));
        }

        let mut bytes = BytesMut::new();
        bytes.put_u32(value.header.checksum);
        bytes.put(checked_bytes);

        Ok(bytes.freeze())
    }
}

trait TryGet {
    fn try_get<const N: usize>(&self, range: Range<usize>) -> Result<[u8; N]>;
}

impl TryGet for Bytes {
    fn try_get<const N: usize>(&self, range: Range<usize>) -> Result<[u8; N]> {
        Ok(self.get(range.clone()).ok_or(MessageConversionError::IncompleteHeader(self.len()))?
            .try_into().expect(format!("Failed to convert bytes {:?} into desired output value.", range).as_str()))
    }
}

fn calc_checksum(data: &[u8]) -> u32 {
    let mut hasher = Hasher::new();
    hasher.update(data);
    hasher.finalize()
}

#[cfg(test)]
mod tests {
    use std::convert::TryInto;
    use std::mem::size_of;

    use bytes::{Buf, BufMut, Bytes, BytesMut};

    use crate::{Header, Message, message::errors::MessageConversionError};

    use super::{CHECK_VALUE, Result};

    const MSG_TYPE: u8 = 42;
    const PAYLOAD: Bytes = Bytes::from_static(b"Hello world");

    #[test]
    fn should_convert_bytes_into_message() {
        let checksum = checksum(MSG_TYPE, CHECK_VALUE, PAYLOAD);
        let bytes = msg_bytes(CHECK_VALUE, checksum, MSG_TYPE, PAYLOAD);

        let message: Message = bytes.try_into().expect("Should have converted bytes into message");
        assert_eq!(message, Message {
            header: Header { checksum, msg_type: MSG_TYPE, check_value: CHECK_VALUE },
            payload: PAYLOAD,
        });
    }

    #[test]
    fn should_convert_header_only_bytes_into_message_with_empty_payload() {
        let checksum = checksum(MSG_TYPE, CHECK_VALUE, Bytes::new());
        let bytes = msg_bytes(CHECK_VALUE, checksum, MSG_TYPE, Bytes::new());

        let message: Message = bytes.try_into().expect("Should have converted bytes into message");
        assert_eq!(message, Message {
            header: Header { checksum, msg_type: MSG_TYPE, check_value: CHECK_VALUE },
            payload: Bytes::new(),
        });
    }

    #[test]
    fn should_convert_message_into_bytes() {
        let checksum = checksum(MSG_TYPE, CHECK_VALUE, PAYLOAD);
        let message = Message {
            header: Header { checksum, msg_type: MSG_TYPE, check_value: CHECK_VALUE },
            payload: PAYLOAD,
        };

        let actual_bytes: Bytes = message.try_into().expect("Should have converted message into bytes");
        assert_eq!(actual_bytes, msg_bytes(CHECK_VALUE, checksum, MSG_TYPE, PAYLOAD));
    }

    #[test]
    fn should_convert_header_only_message_into_bytes_with_no_payload() {
        let checksum = checksum(MSG_TYPE, CHECK_VALUE, Bytes::new());
        let message = Message {
            header: Header { checksum, msg_type: MSG_TYPE, check_value: CHECK_VALUE },
            payload: Bytes::new(),
        };

        let bytes: Bytes = message.try_into().expect("Should have converted message into bytes");
        assert_eq!(bytes, msg_bytes(CHECK_VALUE, checksum, MSG_TYPE, Bytes::new()));
    }

    #[test]
    fn should_fail_to_convert_bytes_to_message_if_there_are_too_few_bytes_for_a_complete_header() {
        let bytes = Bytes::copy_from_slice(&[
            super::calc_checksum(&CHECK_VALUE.to_be_bytes()).to_be_bytes(),
            CHECK_VALUE.to_be_bytes()
        ].concat());

        let result: Result<Message> = bytes.try_into();
        assert_eq!(result.expect_err("Should have failed to convert bytes to message"),
                   // Note: size_of calculation corresponds to each of the fields in the Header struct.
                   MessageConversionError::IncompleteHeader(2 * size_of::<u32>() + size_of::<u8>() - 1))
    }

    #[test]
    fn should_fail_to_convert_bytes_to_message_if_checksum_does_not_match() {
        let checksum = checksum(MSG_TYPE, CHECK_VALUE, PAYLOAD);
        let bytes = msg_bytes(CHECK_VALUE, 90210, MSG_TYPE, PAYLOAD);

        let result: Result<Message> = bytes.try_into();
        assert_eq!(result.expect_err("Should have failed to convert bytes into message"),
                   MessageConversionError::ChecksumMismatch(90210, checksum));
    }

    #[test]
    fn should_fail_to_convert_bytes_to_message_if_check_value_is_incorrect() {
        let checksum = checksum(MSG_TYPE, 90210, PAYLOAD);
        let bytes = msg_bytes(90210, checksum, MSG_TYPE, PAYLOAD);

        let result: Result<Message> = bytes.try_into();
        assert_eq!(result.expect_err("Should have failed to convert bytes into message"),
                   MessageConversionError::WrongCheckValue(90210, CHECK_VALUE));
    }

    #[test]
    fn should_fail_to_convert_message_to_bytes_if_checksum_does_not_match() {
        let checksum = checksum(MSG_TYPE, CHECK_VALUE, PAYLOAD);
        let message = Message {
            header: Header { checksum: 4242, msg_type: MSG_TYPE, check_value: CHECK_VALUE },
            payload: PAYLOAD,
        };

        let result: Result<Bytes> = message.try_into();
        assert_eq!(result.expect_err("Should have failed to convert message into bytes"),
                   MessageConversionError::ChecksumMismatch(4242, checksum));
    }

    #[test]
    fn should_fail_to_convert_message_to_bytes_if_check_value_is_incorrect() {
        let checksum = checksum(MSG_TYPE, 90210, PAYLOAD);
        let message = Message {
            header: Header { checksum, msg_type: MSG_TYPE, check_value: 90210 },
            payload: PAYLOAD,
        };

        let result: Result<Bytes> = message.try_into();
        assert_eq!(result.expect_err("Should have failed to convert message into bytes"),
                   MessageConversionError::WrongCheckValue(90210, CHECK_VALUE));
    }

    fn checksum(msg_type: u8, check_value: u32, payload: Bytes) -> u32 {
        let mut bytes = BytesMut::new();
        bytes.put_u32(check_value);
        bytes.put_u8(msg_type);
        bytes.put(payload);

        super::calc_checksum(bytes.chunk())
    }

    fn msg_bytes(check_value: u32, checksum: u32, msg_type: u8, payload: Bytes) -> Bytes {
        let mut bytes = BytesMut::new();
        bytes.put_u32(checksum);
        bytes.put_u32(check_value);
        bytes.put_u8(msg_type);
        bytes.put(payload);

        bytes.freeze()
    }
}