use bytes::Bytes;
use securefmt::Debug;

#[derive(Debug, PartialEq)]
pub struct Header {
    #[sensitive]
    pub checksum: u32,
    pub msg_type: u8,
    #[sensitive]
    pub check_value: u32,
}

#[derive(Debug, PartialEq)]
pub struct Message {
    pub header: Header,
    #[sensitive]
    pub payload: Bytes,
}

#[cfg(test)]
mod tests {
    use bytes::Bytes;

    use crate::message::{Header, Message};

    #[test]
    #[cfg_attr(feature = "debug", ignore)]
    fn should_protect_sensitive_fields_in_header() {
        let headers = Header {
            checksum: 92,
            msg_type: 42,
            check_value: 86,
        };

        assert_eq!(format!("{:?}", headers), "Headers { checksum: <redacted>, msg_type: 42, check_value: <redacted> }");
    }

    #[test]
    #[cfg_attr(feature = "debug", ignore)]
    fn should_protect_sensitive_fields_in_message() {
        let headers = Header {
            checksum: 92,
            msg_type: 42,
            check_value: 86,
        };
        let message = Message {
            header: headers,
            payload: Bytes::from_static(b"hello"),
        };

        assert_eq!(format!("{:?}", message), "Message { header: Header { checksum: <redacted>, msg_type: 42, check_value: <redacted> }, payload: <redacted> }");
    }
}