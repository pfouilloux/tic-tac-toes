use std::{
    env,
    error::Error,
    fs::File,
    io::Write,
    path::Path,
};

use crc32fast::Hasher;

fn main() -> Result<(), Box<dyn Error>> {
    let out_dir = env::var("OUT_DIR")?;
    let dest_path = Path::new(&out_dir).join("check_value");
    let salt = get_env_or_warn("CHECK_VALUE_SALT", "LOCAL_BUILD");
    let commit_hash = get_env_or_warn("COMMIT_HASH", "LOCAL_BUILD");

    let check_value = calc_check_value(salt, commit_hash);

    File::create(&dest_path)?.write_all(&check_value.to_be_bytes())?;

    Ok(())
}

fn get_env_or_warn(key: &str, default: &str) -> String {
    env::var(key).unwrap_or_else(|_| {
        println!("WARNING!!! Missing {} environment variable. Unless building locally this is likely to be undesirable!", key);
        default.to_string()
    })
}

fn calc_check_value(salt: String, commit_hash: String) -> u32 {
    let mut hasher = Hasher::new();
    hasher.update(salt.as_bytes());
    hasher.update(commit_hash.as_bytes());
    hasher.finalize()
}