#[macro_export]
macro_rules! write_tmp_toml {
    ($file_name:expr, $($arg:tt)*) => {{
        use std::io::Write;

        let config = std::fmt::format(std::format_args!($($arg)*));
        let mut local = tempfile::Builder::new()
            .prefix($file_name)
            .rand_bytes(6)
            .suffix(".toml")
            .tempfile().expect("Should have built named temp file");

        local.write_all(config.as_bytes()).expect("Should have written temp file");

        local
    }}
}