use assert_cmd::{assert::Assert, prelude::{CommandCargoExt, OutputAssertExt}};
use tokio::process::{Child, Command};

pub fn spawn_child_process(name: &str, mut configure_process: impl FnMut(&mut Command) -> &mut Command) -> Child {
    let std_process = std::process::Command::cargo_bin(name)
        .expect("Should have created process");
    let mut process = Command::from(std_process);
    configure_process(process.kill_on_drop(true))
        .spawn()
        .expect("Should have spawned process")
}

pub fn assert_on_process(name: &str, mut configure_process: impl FnMut(&mut assert_cmd::Command) -> &mut assert_cmd::Command) -> Assert {
    let mut cmd = assert_cmd::Command::cargo_bin(name)
        .expect("Failed to construct process");
    configure_process(&mut cmd)
        .assert()
}

pub async fn assert_on(mut process: Child) -> Assert {
    process.kill().await.expect("Should have killed process");
    process.wait_with_output().await.expect("Should have gotten output").assert()
}
