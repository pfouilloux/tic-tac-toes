use std::net::SocketAddr;

use client::Client;
use shared::networking::ToLocalIpv4Addr;

use crate::server::TestServer;

macro_rules! write_tmp_config {
        ($file_name:expr, $($arg:tt)*) => {{
            use std::io::Write;

            let config = std::fmt::format(std::format_args!($($arg)*));
            let mut local = tempfile::Builder::new()
                .prefix($file_name)
                .rand_bytes(6)
                .suffix(".toml")
                .tempfile().expect("Should have built named temp file");

            local.write_all(config.as_bytes()).expect("Should have written temp file");

            local
        }}
    }

#[tokio::test(flavor = "multi_thread", worker_threads = 2)]
async fn should_connect_to_server() {
    TestServer::start(8080).await;
    // TODO: Assert client1 can connect to server
    // TODO: Assert client2 can connect to server
}

struct TestClient;

impl TestClient {
    fn assert_connected_to_server(mut self, server_port: u16) -> Self {
        self.connect(server_port.to_local_addr());
        self
    }
}

impl Client for TestClient {
    fn connect(&mut self, addr: SocketAddr) {
        todo!()
    }
}
