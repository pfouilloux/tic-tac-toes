use crate::{process, server::SERVER_BIN_NAME};

#[test]
fn should_print_help() {
    let assert = process::assert_on_process(SERVER_BIN_NAME, |cmd| cmd.arg("--help"));

    assert
        .success()
        .stdout("server 0.0.0\
                     \nPierre Fouilloux <pfxdev@protonmail.com>\
                     \nTic-tac-toe server\
                     \n\
                     \nUSAGE:\
                     \n    server.exe [OPTIONS]\
                     \n\
                     \nFLAGS:\
                     \n    -h, --help       Prints help information\
                     \n    -V, --version    Prints version information\n\
                     \nOPTIONS:\
                     \n    -c, --config <CONFIG>    Sets a custom config file\n",
        );
}