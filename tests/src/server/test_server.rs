use tokio::{task::JoinHandle, time::Duration};

use crate::{
    process::spawn_child_process,
    server::SERVER_BIN_NAME,
    write_tmp_toml,
};

pub struct TestServer {
    server_handle: JoinHandle<()>
}

impl TestServer {
    pub async fn start(health_port: u16) -> Self {
        let server_handle = start_test_server(health_port).await;
        TestServer {
            server_handle
        }
    }
}

impl Drop for TestServer {
    fn drop(&mut self) {
        self.server_handle.abort();
    }
}

async fn start_test_server(health_port: u16) -> JoinHandle<()> {
    let server_config = write_tmp_toml!("server", "\
            [health]\n
            port = {}\n
        ", health_port);

    let mut server = spawn_child_process(SERVER_BIN_NAME, |cmd|
        cmd.arg(format!("-c {}", server_config.path().display())),
    );

    let server_handle = tokio::spawn(async move { server.wait().await.expect("Should have started"); });
    let ready_handle = tokio::spawn(async move {
        tokio::time::timeout(Duration::from_secs(1), async move {
            let start = std::time::SystemTime::now();
            while !is_server_ready(health_port) && !is_server_live(health_port) {
                println!("Server not ready. Retrying in 100ms...");
                tokio::time::sleep(Duration::from_millis(100)).await;
            }

            println!("Server started in ~{:?}ms", start.elapsed().expect("Failed to get duration!").as_millis());
        }).await.map_err(|e| anyhow::anyhow!("Server did not respond ready in time: {}", e)).unwrap();
    });

    ready_handle.await.expect("Should have been ready");

    server_handle
}

fn is_server_ready(health_port: u16) -> bool {
    if let Ok(response) = ureq::get(format!("http://127.0.0.1:{}/health/ready", health_port).as_str()).call() {
        response.status() == 200
    } else {
        false
    }
}

fn is_server_live(health_port: u16) -> bool {
    if let Ok(response) = ureq::get(format!("http://127.0.0.1:{}/health/live", health_port).as_str()).call() {
        response.status() == 200
    } else {
        false
    }
}