pub use test_server::TestServer;

mod cli_tests;
mod test_server;

const SERVER_BIN_NAME: &str = "server";